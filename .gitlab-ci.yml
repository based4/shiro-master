# https://gitlab.com/freedumbytes/setup/blob/master/.gitlab-ci.yml
# https://freedumbytes.bitbucket.io/dpl/html/continuous.integration.pipeline.html#continuous.integration.pipeline.gitlab
stages:
  - compile
  - test
  - package
  - integration
  - docs
  - deploy

# output_limit Set maximum build log size in kilobytes, by default set to 4096 (4MB)
# runner:output_limit: 8192

variables:
  # https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#using-the-overlayfs-driver https://blog.sparksuite.com/7-ways-to-speed-up-gitlab-ci-cd-times-29f60aab69f9
  DOCKER_DRIVER: overlay2
  # https://hub.docker.com/_/maven
  MAVEN_DOCKER_IMAGE: "maven:3.6.0-jdk-8-alpine"
  MAVEN_SHOW_TIME: "-Dorg.slf4j.simpleLogger.showDateTime=true"
  MAVEN_FORMAT_TIME: "-Dorg.slf4j.simpleLogger.dateTimeFormat=EEE..yyyy-MM-dd...HH:mm:ss.SSS..z"
  MAVEN_SUPPRESS_DOWNLOAD_LOGS: "-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN"
  MAVEN_PREVENT_RECOMPILE: "-Dmaven.compiler.useIncrementalCompilation=false"
  MAVEN_OPTS: "-Djava.awt.headless=true $MAVEN_SHOW_TIME $MAVEN_FORMAT_TIME $MAVEN_SUPPRESS_DOWNLOAD_LOGS"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-fast --show-version -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository $MAVEN_PREVENT_RECOMPILE"
  # This will supress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  # MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  #MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true -B -Pjdk8"
  #TOMCAT_HOME: ""
  MYSQL_8_DOCKER_IMAGE: "mysql-server:8.0:latest"

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
cache:
  paths:
    - .m2/repository
    
.maven-compile: &maven-compile
  stage: compile
  script:
    - 'mvn $MAVEN_CLI_OPTS test-compile'
  artifacts:
    when: on_success
    paths:
      - "target"
      - "*/target"
      - "*/*/target"
      - "*/*/*/target"
      - "src/main/generated"
      - "*/src/main/generated"
      - "*/*/src/main/generated"
      - "*/*/*/src/main/generated"
    expire_in: 1h
  tags:
    - docker
    
# Surefire https://maven.apache.org/surefire/maven-surefire-plugin/
.maven-unit: &maven-unit
  stage: test
  script:
    - 'mvn $MAVEN_CLI_OPTS test'
  artifacts:
    when: on_failure
    reports:
      junit:
      - "*/target/surefire-reports/*"
      - "*/*/target/surefire-reports/*"
      - "*/*/*/target/surefire-reports/*"
      - "*/*/*/*/target/surefire-reports/*"
    # https://gitlab.com/gitlab-org/gitlab-runner/issues/2340 
    expire_in: 2 years
  tags:
    - docker

.maven-package: &maven-package
  stage: package
  script:
    - mvn --batch-mode $MAVEN_PREVENT_RECOMPILE install -Pjdk8 -B
    - zip -r shiro ./target
  artifacts:
    when: on_success
    expire_in: 1 month
    paths:
      - shiro.zip
  tags:
    - docker
    
# Failsafe https://maven.apache.org/surefire/maven-failsafe-plugin/index.html
.maven-integration: &maven-integration
  stage: integration
  script:
    - 'mvn $MAVEN_CLI_OPTS failsafe:integration-test jacoco-maven-plugin:merge'
  artifacts:
    when: on_failure
    paths:
      - "target/failsafe-reports/*"
      - "*/target/failsafe-reports/*"
      - "*/*/target/failsafe-reports/*"
      - "target/coverage-reports/*"
      - "*/target/coverage-reports/*"
      - "*/*/target/coverage-reports/*"
    expire_in: 1h
  tags:
    - docker

.maven-site: &maven-site
  stage: docs
  script:
    - 'mvn $MAVEN_CLI_OPTS site:stage'
    - 'rm -r target/site'
    - 'mv `find ./target -type f -name "team.html" | sed -r "s|/[^/]+$||" | sort -u | head -1` target/pages'
  artifacts:
    paths:
      - "target/pages"
    expire_in: 1h
  tags:
    - docker
    
maven-compile:job:
  <<: *maven-compile
  image: $MAVEN_DOCKER_IMAGE

maven-unit:job:
  <<: *maven-unit
  image: $MAVEN_DOCKER_IMAGE
  dependencies:
    - maven-compile:job

maven-package:job:
  <<: *maven-package
  image: $MAVEN_DOCKER_IMAGE
  dependencies:
    - maven-unit:job
    
maven-integration:job:
  <<: *maven-integration
  image: $MAVEN_DOCKER_IMAGE
  dependencies:
    - maven-package:job

maven-site:job:
  <<: *maven-site
  image: $MAVEN_DOCKER_IMAGE
  artifacts:
    when: on_failure
    paths:
      - "target/pages"
    expire_in: 1 day
  dependencies:
    - maven-package:job

pages:
  stage: deploy
  script:
  - 'mkdir .public'
  - 'cp -r target/pages/* .public'
  - 'mv .public public'
  dependencies:
    - maven-site:job
  artifacts:
    paths:
    - public
    expire_in: 1 month

    

